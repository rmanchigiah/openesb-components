package com.sun.jbi.engine.bpel.core.bpel.util;

import org.w3c.dom.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public interface BpelCache {
    
    String put(String key, Node value);
    
    Node get(String key);
    
    Node remove(String key);
}
