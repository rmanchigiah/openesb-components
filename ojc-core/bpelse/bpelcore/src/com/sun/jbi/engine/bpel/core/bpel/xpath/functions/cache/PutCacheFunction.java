package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.cache.CacheManager;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.ri.EvalContext;
import org.w3c.dom.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class PutCacheFunction extends AbstractCacheFunction {

    public PutCacheFunction(CacheManager cacheManager) {
        super(cacheManager);
    }
        
    public Object invoke(ExpressionContext context, Object[] parameters) {
        Node input = (Node) convertParam(parameters[1]);
        return cacheManager.put((String) BPWSFunctions.convertParam(parameters[0]), 
                input);
    }
    
    private Object convertParam(Object src) {
        if (src instanceof Pointer) {
            return ((Pointer) src).getNode();
        }
        else if (src instanceof EvalContext) {
            return convertParam(((EvalContext) src).getCurrentNodePointer());
        }
        else if (src instanceof Node) { 
            //bug 615 input from doXSLTransform fuction is a Node
            return src;
        }
        return null;
    }
}
