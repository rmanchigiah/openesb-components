package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.cache.CacheManager;
import org.apache.commons.jxpath.Function;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public abstract class AbstractCacheFunction implements Function {
    
    protected final CacheManager cacheManager;
    
    public AbstractCacheFunction(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
}
